/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.1.1.2                               |
|   \\  /    A nd           | Web:      www.OpenFOAM.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      controlDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

application     pimpleFoam;

startFrom       latestTime;

startTime       0;

stopAt          endTime;

endTime         2;

deltaT          1e-5;

writeControl    runTime;

writeInterval   0.5;

purgeWrite      0;

writeFormat     binary;

writePrecision  6;

writeCompression false;

timeFormat      general;

timePrecision   8;

runTimeModifiable on;

adjustTimeStep  no;

maxCo           0.9;

maxAlphaCo      0.9;

maxDeltaT       1e-4;

libs (
    "libWilcoxOmegaWallFunction.so"
    "libsimpleSwakFunctionObjects.so"
    "libswakFunctionObjects.so"    
);

functions
{
    forces
    {
        type            forceCoeffs;
        libs            ("libforces.so");
        writeControl    timeStep;

        patches
        (
           cylinder 
        );
        // Field names
        p               p;
        U               U;
        rho             rhoInf;

        rhoInf      1.292;
        CofR        (0 0 0);
        liftDir     (0 1 0);
        dragDir     (1 0 0);
        pitchAxis   (0 0 1);
        magUInf     10;
        lRef        0.04;
        Aref        4e-5;
    }
    wallShearStress
    {
        type            wallShearStress;
        libs            ("libfieldFunctionObjects.so");
	patch
        (
	    bottom
	);
        executeControl  timeStep;
        writeControl    writeTime;
    }
    fieldAverage
    {
        type               fieldAverage;
        libs               (fieldFunctionObjects);
        enabled            true;
        writeControl       outputTime;
        restartOnRestart   false;
        restartOnOutput    false;
        periodicRestart    false;
        
    fields
    (
        U
        {
            mean        yes;
            prime2Mean  yes;
            base        time;
        }
    );
    }
    probes
    {
            type probes; // Type of functionObject
                
            probeLocations // Locations to be probed. runTime modifiable!
            (
                (0.04 0.02  0.0005)
                (0.04 0.025 0.0005)
                (0.08 0.02  0.0005)
                (0.08 0.025 0.0005)
            );
            // Fields to be probed. runTime modifiable!
            fields
            (
                    U
            );
    }
    yPlus1
    {
        type yPlus;
        libs (fieldFunctionObjects);
        
        writePrecision 5;	
        writeControl writeTime;
    }
    linesample           
    { 
        type                      sets; 
        libs                      (libsampling);
        writeControl              writeTime;
        interpolationScheme       cellPoint;
 
        setFormat   raw; 

        sets 
        ( 
            center_line
            { 
                type            uniform; 
                axis            x; 
                start           (-0.2 0 0.0005); 
                end             ( 0.8 0 0.0005); 
                nPoints         200; 
            } 
            line_006
            { 
                type            uniform; 
                axis            y; 
                start           (0.06 -0.2 0.0005); 
                end             (0.06  0.2 0.0005); 
                nPoints         200; 
            }
        );
        fields
        (
            UMean
            UPrime2Mean
        ); 
     } 
}

// ************************************************************************* //
