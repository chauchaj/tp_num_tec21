'''
Author : Matthias Renaud


Generate a openFoam blockMeshDict file for 2D or 3D flow around a cylinder.
The differents blocks and vertices are described below.
The mesh is compound of one or more layer of the 12 blocks described below.
0-19 stand vertices, b0-b11 for blocks

  y ^        0 - - - - - 1- - - - - - -2 - - - - - 3
    |        |           |             |           |  
    --> x    |     b0    |      b1     |     b2    |  ny  
             |           |             |           |
             4 - - - - - 5 - - - - - - 6 - - - - - 7
             |           | \    b9  /  |           |
             |           |  16 ___ 17  |           |
             |           |    /   \    |           |  
             |     b3    | b8|     |b10|     b4    |  nc  
             |           |    \___/    |           |
             |           |  18     19  |           |
             |           | /   b11  \  |           |
             8 - - - - - 9 - - - - - -10 - - - - -11
             |           |             |           |  
             |     b5    |      b6     |     b7    |  ny  
             |           |             |           |
             12 - - - - 13 - - - - - -14 - - - - - 15
                 nxup           nc         nxdo  
'''                                                   

from fluidfoam.meshdesign import getgz, getdzs
from math import sqrt
import matplotlib.pyplot as plt
from math import pi

nb_dec = 5 # number of decimal value to consider
pathBlockMeshDict = './system/' # path where to write blockMeshDict

# ----- domain properties, can be modified ----- #
# domain dimensions
D = 0.04    # cylinder diameter
W = 10      # domain width in cylinder diameter unit
xUp = 5     # domain length upstream from cylinder in cylinder diameter unit
xDo = 20    # domain length, downstream from cylinder in cylinder diamter unit
# mesh dimensions, cylindrical area
dr = 0.001 # distance of first cell center from cylinder wall, meter
nr = 32     # number of cell in the radial direction in the cylindrical area (center area)
# mesh dimensions, outline areas
nxup = 8   # number of cells upstream from cylindrical area, x-direction
nxdo = 64   # number of cells downstream from cylindrical area, x-direction
ny = 16     # number of cell on sides, y-direction
# vertical dimensions
Zvalues = [0.,0.001] # list of vertical plane coordinates, at least 2 values for one blocks layer
NZ = [1]        # list of number of vertical cells in a block
GZ = [1]        # list of vertical grading values in a block
# bottom and surface patches, empty if 2D grid
bottomPatch = 'empty'
surfacePatch = 'empty'

# ------ other values, not to be modified ------ #
if len(NZ)!=len(GZ) :
	raise ValueError(f'NZ and GZ must have same length, NZ : {len(NZ)}, GZ : {len(GZ)}')
elif len(Zvalues)!=len(NZ)+1 :
	raise ValueError(f'Zvalues must have one more element than NZ, Zvalues : {len(Zvalues)}, NZ : {len(NZ)}')

xUp = round(-xUp*D,nb_dec) # domain upstream length
xDo = round(xDo*D,nb_dec) # domain downstream length
W = round(W*D,nb_dec) # domain width
R = round(D/2,nb_dec) # cylinder radius
w = round(W/2,nb_dec)
Rinner = round(D/(2*sqrt(2)),nb_dec)
Router = round(2*D,nb_dec)
RcylArea = round(sqrt(2)*Router,nb_dec)
gr = round(getgz(RcylArea-R,2*dr,nr)[2],nb_dec) # grading value in radial direction
drout = round(getdzs(RcylArea-R,gr,nr)[1],nb_dec) # length last cells radial direction
gxup = round(1/getgz(-xUp-RcylArea,drout,nxup)[2],nb_dec)
gxdo = round(getgz(xDo-RcylArea,drout,nxdo)[2],nb_dec)
gysup = round(getgz(w-RcylArea,drout,ny)[2],nb_dec)
gyinf = round(1/gysup,nb_dec)
nc = int((0.5*pi/sqrt(2))*(RcylArea/drout)) # fourth of the number of cells around cylinder
if (nc%2==1):
    nc = nc + 1
print(f'\n cells number around cylinder = {4*nc}')

ar, aup, ado, ay = gr**(1/(nr-1)), gxup**(1/(nxup-1)), gxdo**(1/(nxdo-1)), gysup**(1/(ny-1))
print(f'\ncommon ratio : cylinder : {ar}, upstream : {aup}, downstream : {ado}, sides : {ay}')
print('ratio should be between 0.9 and 1.1, modify value of nr, nxup, nxdo and ny to affect the common ratios')

####### tools #######

def edit_vert(block,i) :
	n = 20*i
	new_hex = tuple([vert+n for vert in block['hex']])
	block['hex'] = new_hex

def trans_face(face,i) :
	n = i*20
	return tuple([vert+n for vert in face])

def all_faces(faces) :
	new_faces = []
	for face in faces :
		for i in range(len(NZ)) :
			new_faces.append(trans_face(face,i))
	return new_faces

####### vertices #######

vertices = []

for z in Zvalues :
	vertices.append((xUp, w, z)) #0
	vertices.append((-Router,w,z)) #1
	vertices.append((Router,w,z)) #2
	vertices.append((xDo,w,z)) #3
	vertices.append((xUp,Router,z)) #4
	vertices.append((-Router,Router,z)) #5
	vertices.append((Router,Router,z)) #6
	vertices.append((xDo,Router,z)) #7
	vertices.append((xUp,-Router,z)) #8
	vertices.append((-Router,-Router,z)) #9
	vertices.append((Router,-Router,z)) #10
	vertices.append((xDo,-Router,z)) #11
	vertices.append((xUp,-w,z)) #12
	vertices.append((-Router,-w,z)) #13
	vertices.append((Router,-w,z)) #14
	vertices.append((xDo,-w,z)) #15
	# cylinder vertices
	vertices.append((-Rinner,Rinner,z)) #16
	vertices.append((Rinner,Rinner,z)) #17
	vertices.append((-Rinner,-Rinner,z)) #18
	vertices.append((Rinner,-Rinner,z)) #19
	# vertices for arc edges


####### Blocks #######
blocks = []

for nz,gz in zip(NZ,GZ) :
	blocks.append({'hex':(4,5,1,0,24,25,21,20),'n':(nxup,ny,nz),'gz':(gxup,gysup,gz)}) #0
	blocks.append({'hex':(5,6,2,1,25,26,22,21),'n':(nc,ny,nz),'gz':(1,gysup,gz)}) #1
	blocks.append({'hex':(6,7,3,2,26,27,23,22),'n':(nxdo,ny,nz),'gz':(gxdo,gysup,gz)}) #2
	blocks.append({'hex':(8,9,5,4,28,29,25,24),'n':(nxup,nc,nz),'gz':(gxup,1,gz)}) #3
	blocks.append({'hex':(10,11,7,6,30,31,27,26),'n':(nxdo,nc,nz),'gz':(gxdo,1,gz)}) #4
	blocks.append({'hex':(12,13,9,8,32,33,29,28),'n':(nxup,ny,nz),'gz':(gxup,gyinf,gz)}) #5
	blocks.append({'hex':(13,14,10,9,33,34,30,29),'n':(nc,ny,nz),'gz':(1,gyinf,gz)}) #6
	blocks.append({'hex':(14,15,11,10,34,35,31,30),'n':(nxdo,ny,nz),'gz':(gxdo,gyinf,gz)}) #7
	blocks.append({'hex':(16,5,9,18,36,25,29,38),'n':(nr,nc,nz),'gz':(gr,1,gz)}) #8
	blocks.append({'hex':(17,6,5,16,37,26,25,36),'n':(nr,nc,nz),'gz':(gr,1,gz)}) #9
	blocks.append({'hex':(19,10,6,17,39,30,26,37),'n':(nr,nc,nz),'gz':(gr,1,gz)}) #10
	blocks.append({'hex':(18,9,10,19,38,29,30,39),'n':(nr,nc,nz),'gz':(gr,1,gz)}) #11

j = 0
for block in blocks :
	iblock = j//12	
	edit_vert(block,iblock)
	j += 1		

####### Edges #######

edges = []

i = 0
for z in Zvalues :
	iv = i*20
	edges.append({'type':'arc','vertices':(5+iv,6+iv),'point':(0,RcylArea,z)})
	edges.append({'type':'arc','vertices':(6+iv,10+iv),'point':(RcylArea,0,z)})
	edges.append({'type':'arc','vertices':(10+iv,9+iv),'point':(0,-RcylArea,z)})
	edges.append({'type':'arc','vertices':(9+iv,5+iv),'point':(-RcylArea,0,z)})
	edges.append({'type':'arc','vertices':(16+iv,17+iv),'point':(0,R,z)})
	edges.append({'type':'arc','vertices':(17+iv,19+iv),'point':(R,0,z)})
	edges.append({'type':'arc','vertices':(19+iv,18+iv),'point':(0,-R,z)})
	edges.append({'type':'arc','vertices':(18+iv,16+iv),'point':(-R,0,z)})
	i+=1

####### Patches #######

patches = []

faces_inlet = [(4,24,20,0),(8,28,24,4),(12,32,28,8)]
faces_outlet = [(7,3,23,27),(11,7,27,31),(15,11,31,35)]
faces_lateral = [(1,0,20,21),(2,1,21,22),(3,2,22,23),(12,13,33,32),(13,14,34,33),(14,15,35,34)]
faces_cylinder = [(16,17,37,36),(17,19,39,37),(19,18,38,39),(18,16,36,38)]
faces_bottom = [(0,1,5,4),(1,2,6,5),(2,3,7,6),(4,5,9,8),(6,7,11,10),(8,9,13,12),(9,10,14,13),(10,11,15,14),(5,16,18,9),(5,6,17,16),(17,6,10,19),(18,19,10,9)]
faces_surface = [trans_face(face,len(NZ)) for face in faces_bottom]

patches.append({'name':'inlet','type':'patch','faces':all_faces(faces_inlet)})
patches.append({'name':'outlet','type':'patch','faces':all_faces(faces_outlet)})
patches.append({'name':'lateral','type':'symmetry','faces':all_faces(faces_lateral)})
patches.append({'name':'cylinder','type':'wall','faces':all_faces(faces_cylinder)})
patches.append({'name':'bottom','type':bottomPatch,'faces':faces_bottom})
patches.append({'name':'surface','type':surfacePatch,'faces':faces_surface})

####### print functions #######
def write_header() :
	strout = 'FoamFile\n{\n'
	strout += '\tversion\t2.0;\n'
	strout += '\tformat\tascii;\n'
	strout += '\tclass\tdictionary;\n'
	strout += '\tobject\tblockMeshDict;\n}\n\n'
	strout += 'scale\t1.0;\n\n'
	return strout
def write_vertices(vertices) :
	strout = 'vertices\n(\n'
	i = -1
	for vert in vertices :
		i += 1
		strout += '\t'+str(vert).replace(',',' ') +f'  //{i}\n'
	strout += ');\n\n'
	return strout
# -------------------------- #
def write_block(block) :
	strout = '\t' + 'hex '+str(block['hex']).replace(',',' ')
	strout += ' '+str(block['n']).replace(',',' ')
	strout += ' simpleGrading ' + str(block['gz']).replace(',',' ') + '\n'
	return strout

def write_blocks(blocks) :
	strout = 'blocks\n(\n'
	for b in blocks :
		strout += write_block(b)
	strout += ');\n\n'
	return strout
# -------------------------- #
def write_edge(edge) :
	strout = '\t' + edge['type'] + ' ' + str(edge['vertices']).strip('()').replace(',',' ')
	strout += ' ' + str(edge['point']).replace(',',' ') +'\n'
	return strout

def write_edges(edges) :
	strout = 'edges\n(\n'
	for edge in edges :
		strout += write_edge(edge)
	strout += ');\n\n'
	return strout
# -------------------------- #
def write_patch(patch) :
	strout = '\t' + patch['type'] + ' ' + patch['name'].replace(',',' ') + '\n\t(\n'
	for face in patch['faces'] :
		strout += '\t\t' + str(face).replace(',',' ') + '\n'
	strout += '\t)\n'
	return strout

def write_patches(patches) :
	strout = 'patches\n(\n'
	for patch in patches :
		strout += write_patch(patch)
	strout += '\n);\n\n'
	return strout

def write_merge() :
	strout = 'mergPatchPairs\n'
	strout += '(\n'
	strout += ');\n'
	return strout
# -------------------------- #

pathFile = pathBlockMeshDict + 'blockMeshDict'
def write_blockMesh(path) :
	with open(path,'w') as file :
		file.write(write_header())
		file.write(write_vertices(vertices))
		file.write(write_blocks(blocks))
		file.write(write_edges(edges))
		file.write(write_patches(patches))
		file.write(write_merge())
write_blockMesh(pathFile)
