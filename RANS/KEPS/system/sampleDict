/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  1.7.1                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/

FoamFile
{
    version         2.0;
    format          ascii;
    class           dictionary;
    location        system;
    object          sampleDict;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

interpolationScheme cell;

// Set output format : choice of
//      xmgr
//      jplot
//      gnuplot
//      raw
setFormat raw;

// Surface output format. Choice of
//      null        : suppress output
//      foamFile    : separate points, faces and values file
//      dx          : DX scalar or vector format
//      vtk         : VTK ascii format
//      raw         : x y z value format for use with e.g. gnuplot 'splot'.
//
// Note:
// other formats such as obj, stl, etc can also be written (by proxy)
// but without any values!
surfaceFormat raw;

// interpolationScheme. choice of
//      cell          : use cell-centre value only; constant over cells (default)
//      cellPoint     : use cell-centre and vertex values
//      cellPointFace : use cell-centre, vertex and face values.
// 1] vertex values determined from neighbouring cell-centre values
// 2] face values determined using the current face interpolation scheme
//    for the field (linear, gamma, etc.)




// Set sampling definition: choice of
//      uniform             evenly distributed points on line
//      face                one point per face intersection
//      midPoint            one point per cell, inbetween two face intersections
//      midPointAndFace     combination of face and midPoint
//
//      curve               specified points, not nessecary on line, uses
//                          tracking
//      cloud               specified points, uses findCell
//
// axis: how to write point coordinate. Choice of
// - x/y/z: x/y/z coordinate only
// - xyz: three columns
//  (probably does not make sense for anything but raw)
// - distance: distance from start of sampling line (if uses line) or
//             distance from first specified sampling point
//
// type specific:
//      uniform, face, midPoint, midPointAndFace : start and end coordinate
//      uniform: extra number of sampling points
//      curve, cloud: list of coordinates
sets
(
    centerPatch_neg
    {
        type        uniform;
        axis        x;
        //- cavity. Slightly perturbed so not to align with face or edge.
        start       (-0.5 0.0001  0.05);           
        end         (-0.05 0.0001 0.05);
        nPoints     400;
    }
    centerPatch_pos
    {
        type        uniform;
        axis        x;
        //- cavity. Slightly perturbed so not to align with face or edge.
        start       (0.05 0.0001  0.05);           
        end         (0.5 0.0001 0.05);
        nPoints     400;
    }
    centerPatch_zp
    {
        type        uniform;
        axis        z;
        //- cavity. Slightly perturbed so not to align with face or edge.
        start       (-0.49 0.3  -0.1);           
        end         (-0.49 0.3 0.2);
        nPoints     400;
    }
    centerPatch_z
    {
        type        uniform;
        axis        z;
        //- cavity. Slightly perturbed so not to align with face or edge.
        start       (-0.25 0.3  -0.1);           
        end         (-0.25 0.3 0.2);
        nPoints     400;
    }

);

// Fields to sample.
fields
(
      Ua
//      p
);

// Surface sampling definition: choice of
//      plane : values on plane defined by point, normal.
//      patch : values on patch.
//
// 1] patches are not triangulated by default
// 2] planes are always triangulated
// 3] iso-surfaces are always triangulated



// *********************************************************************** //
