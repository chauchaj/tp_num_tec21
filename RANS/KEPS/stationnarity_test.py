#!/usr/bin python3

# script to test convergence of mean fields based on linesample

import numpy as np
import matplotlib.pyplot as plt

pathSamples = './postProcessing/linesample/'

nameUp = 'centerPatch_neg_0_05_U_UMean.xy'
timeList = ['60','80','100']

Umean=[]


for time in timeList:
    nameOpen = pathSamples+time+'/'+nameUp
    Umean.append(np.loadtxt(nameOpen,unpack=True,usecols=1))

print(len(Umean))
fig,ax = plt.subplots()
for U,t in zip(Umean,timeList):
    ax.plot(U,label=t)
ax.legend()
fig.show()
plt.show()
