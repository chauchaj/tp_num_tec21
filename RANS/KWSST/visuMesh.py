#!usr/bin/env python3

from fluidfoam import meshvisu
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection

myMesh = meshvisu.MeshVisu(path = './')

fig, ax = plt.subplots()
ln_coll = LineCollection(myMesh.get_all_edgesInBox(),linewidths=0.25)
ax.add_collection(ln_coll,autolim=False)
ax.set_xlim(myMesh.get_xlim())
ax.set_ylim(myMesh.get_ylim())
ax.set_aspect('equal')
ax.axis('off')
plt.show()
# plt.savefig('./mesh.svg',dpi=fig.dpi,transparent=True)
