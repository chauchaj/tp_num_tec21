#!/usr/bin/env python3

'''
This scripts is used to plot spectrum of velocity fluctuations and lift/drag coef.
fluctuations from an OpenFOAM simulation.
'''

import numpy as np
import matplotlib.pyplot as plt
from fluidfoam.readpostpro import readprobes,readforce
from scipy.fft import fft,fftfreq
import scipy.signal as spsig
from scipy.signal.windows import hann

# cylinder diameter, velocity compnents to use and bottom surface condition
D = 0.04
U0 = 10
fExp = 51

configName = 'URANSO1'
#configName = 'URANSO2'
#configName = 'LES'

if configName == 'URANSO1':
	pathCase = '../URANS/ORDER1/KWSST/'
	timestart = 0.                   # Start time for sampling from time
elif configName == 'URANSO2':
	pathCase = '../URANS/ORDER2/KWSST/'
	timestart = 0.                   # Start time for sampling from time
elif configName == 'LES':
	pathCase = '../LES/LEScylinder/'
	timestart = 0.                   # Start time for sampling from time

##### PARAMETERS #####
fmax = 500                          # max frequency to represent on Fourier plot
force_file_name = 'coefficient'     # Variable on which fft is applied
index_Cl = 4                        # Lift force
index_Cd = 1                        # Drag force
n0 = 0                              # Start point for the fft
nfft = 1024                         # Number of point for the fft
fftdeltat = 0.0005                   # Sampling Delta t for the fft, note that Tfft = nfft * fftdeltat =< Ttot = endTime - time
FFT_velocity = 1

### saving plot parameters ###
savePlot = False # True if plot to be saved else False
pathSavePlot = '../Figures/' # path of plot for saving
nameSavePlot1 = configName+'TimeSeriesCdCl.png' # name of png file to save plot
nameSavePlot2 = configName+'SpectrumCdCl.png' # name of png file to save plot
nameSavePlot3 = configName+'TimeSeriesV.png' # name of png file to save plot
nameSavePlot4 = configName+'SpectrumV.png' # name of png file to save plot

probes_name = 'probes'
time_name = '0'

###################
#
# Read force data
#
###################
# reading Data
force = readforce(pathCase, namepatch = 'forces', 
					time_name = 'mergeTime', name = force_file_name)
timevec = np.zeros(len(force))
varC = np.zeros((len(force),2))
for i in range(len(force)):
    timevec[i] = force[i,0]
    varC[i,0] = force[i,index_Cl]
    varC[i,1] = force[i,index_Cd]

# Sampling
i = 0
calculdeltat = round(timevec[1]-timevec[0],8) # simulation time step value

if (i*fftdeltat/calculdeltat+timestart/calculdeltat >= len(force)):
	print("Error: your signal is not long enough for your choice of fft parameters")

varCi = np.zeros((nfft,2))
vart = np.zeros(nfft)
while i < nfft:
    for k in range(2):
        varCi[i,k] = varC[int(i*fftdeltat/calculdeltat+timestart/calculdeltat), k]
    vart[i] = timevec[int(i*fftdeltat/calculdeltat+timestart/calculdeltat)]
    i += 1

# fft calculation
y1 = np.zeros((nfft,2),dtype = np.complex128)
for k in range(2):
# Division by nfft to correct the spectral amplitude
    y1[:, k] = np.fft.fft(varCi[n0:n0+nfft,k] - np.mean(varCi[n0:n0+nfft,k]))/nfft
##
## Plotting figure 
##
# Temporal plot section
colorList = ['r', 'b']
labelList = ['Lift', 'Drag']

fig1 = plt.figure(figsize=(16, 8))

for k in range(2):
    plt.plot(vart[n0:n0+nfft], varCi[n0:n0+nfft,k],
         color = colorList[k], label = labelList[k])
plt.legend(loc='best')
plt.xlabel('time (s)')
plt.ylabel('Coefficient (-)')
plt.show(block=False)

# frequency plot section
f = np.arange(nfft)*1/(fftdeltat*nfft)

fig2 = plt.figure(figsize=(16, 8))
fmaxCl = f[np.where(y1[n0:n0+nfft//2,0]==np.amax(y1[n0:n0+nfft//2,0]))][0]
StrouhalCl = fmaxCl*D/U0
print('Strouhal Cl:',round(StrouhalCl,3))
plt.title(r'$f_{max}$ ='+str(round(fmaxCl,3))+' Hz')
for k in range(2):
    plt.plot(f[n0:n0+nfft//2], abs(y1[n0:n0+nfft//2,k]), color = colorList[k], label = labelList[k])
plt.plot([fExp,fExp], [0,np.max(abs(y1[n0:n0+nfft//2]))], color = 'k', 
                     ls = '--', label = 'Exp (f=51 Hz)')
plt.grid()
plt.axis([0.0, fmax, 0,  1.1*np.max(abs(y1[n0:n0+nfft//2]))])
plt.yscale('linear')
plt.legend(loc='upper right')
plt.xlabel(r'f (Hz)')
plt.ylabel(r'Amplitude ($m^2/s^2$)')

######################
#
#  Read velocity probes
#
######################
if FFT_velocity==1:
    data_probes = readprobes(pathCase,probes_name,'mergeTime','U')
    timeArr = data_probes[1]
    ntimeStep = len(timeArr) # probes data time step
    dt = round(timeArr[1]-timeArr[0],8) # simulation time step value
    
    nprobes = len(data_probes[0])
    #read V component
    probesList = [data_probes[2][:,i,1] for i in range(nprobes)]
    
    # Sampling
    i = 0
    
    varUi = np.zeros((nfft,nprobes))
    varUt = np.zeros(nfft)
    while i < nfft:
        for k in range(nprobes):
            varUi[i,k] = data_probes[2][int(i*fftdeltat/dt+timestart/dt),k,1]
        varUt[i] = timeArr[int(i*fftdeltat/dt+timestart/dt)]
        i += 1
    
    # fft calculation
    yU1 = np.zeros((nfft,nprobes),dtype = np.complex128)
    for k in range(2):
    # Division by nfft to correct the spectral amplitude
        yU1[:, k] = np.fft.fft(varUi[n0:n0+nfft,k] 
                               - np.mean(varUi[n0:n0+nfft,k]))/nfft

    fig3 = plt.figure(figsize=(16, 8))
    for k in range(nprobes):
        plt.plot(varUt[n0:n0+nfft//2], varUi[n0:n0+nfft//2,k],
                  label = 'Probe N'+str(k))
        plt.legend(loc='best')
        plt.xlabel('time (s)')
        plt.ylabel('V (m/s)')
    
    fig4 = plt.figure(figsize=(16, 8))
    fmaxV = f[np.where(yU1[n0:n0+nfft//2,1]==np.amax(yU1[n0:n0+nfft//2,1]))][0]
    StrouhalV = fmaxV*D/U0
    print('Strouhal V:', round(StrouhalV,3))
    plt.title(r'$f_{max}$ ='+str(round(fmaxV,3))+' Hz')
    for k in range(nprobes):
        plt.plot(f[n0:n0+nfft//2], abs(yU1[n0:n0+nfft//2,k]), label = 'Probe N'+str(k))
    plt.plot([fExp,fExp], [0,np.max(abs(yU1[n0:n0+nfft//2]))], 
                    color = 'k', ls = '--', label = 'Exp (f=51 Hz)')
    
    plt.grid()
    plt.axis([0.0, fmax, 0,  1.1*np.max(abs(yU1[n0:n0+nfft//2]))])
    plt.yscale('linear')
    plt.legend(loc='upper right')
    plt.xlabel(r'f (Hz)')
    plt.ylabel(r'Amplitude ($m^2/s^2$)')

plt.show()

if savePlot :
    fig1.savefig(pathSavePlot+nameSavePlot1,dpi=150)
    fig2.savefig(pathSavePlot+nameSavePlot2,dpi=150)
    if FFT_velocity==1:
        fig3.savefig(pathSavePlot+nameSavePlot3,dpi=150)
        fig4.savefig(pathSavePlot+nameSavePlot4,dpi=150)


