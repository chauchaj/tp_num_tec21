#!/usr/bin/env python3

'''
This scripts is used to compare Reynolds shear stress profiles between two OpenFOAM simulations.
'''

import os
import sys

import numpy as np
from fluidfoam import readof as rdf
from pylab import figure, subplot, axis, xlabel, ylabel, show, savefig, plot
from pylab import title, matplotlib, legend, grid
#
# Functions
#
def readOFprofiles(pathFoamDir,time_name):
	pathCenter = pathFoamDir+'postProcessing/linesample/'+time_name+'/center_line_UMean_UPrime2Mean.xy'
	data = np.loadtxt(pathCenter,unpack=True)
	data = np.array(data)
	xc = data[0,:]
	uuc = data[4,:]
	del data
	pathLine006 = pathFoamDir+'postProcessing/linesample/'+time_name+'/line_006_UMean_UPrime2Mean.xy'
	data = np.loadtxt(pathLine006,unpack=True)
	data = np.array(data)
	yl = data[0,:]
	uul = data[4,:]
	del data
	
	return xc, uuc, yl, uul

########## PARAMETERS AND SETTINGS, SET BY USER ###########

# cylinder diameter, velocity compnents to use and bottom surface condition
D = 0.04
U0 = 10

#
configName = 'RANS'
#configName = 'RANS_URANS'
#configName = 'URANS_LES'

if configName == 'RANS':
	basepath='../RANS/'
	caseList = ['KWSST/','KEPS/']
	timeList = ['1481','30511']
	UnameList = ['U','U']
	labelList = ['k-omega SST RANS','k-epsilon RANS']
elif configName == 'RANS_URANS':
	basepath='../'
	caseList = ['RANS/KWSST/','URANS/ORDER2/KWSST/']
	timeList = ['1481','1']
	UnameList = ['U','UMean']
	labelList = ['k-omega SST RANS','k-omega SST URANS']
elif configName == 'URANS_LES':
	basepath='../'
	caseList = ['URANS/ORDER2/KWSST/','LES/RUN1/']
	timeList = ['1','1']
	UnameList = ['UMean','UMean']
	labelList = ['k-omega SST URANS','LES']

lineList = ['-','--','-.',':','-']
colorList = ['r','b','g','m','c']

### saving plot parameters ###
savePlot = False # True if plot to be saved else False
pathSavePlot = '../Figures/' # path of plot for saving
nameSavePlot1 = configName+'centerlineU_ReynolsStressProfile.png' # name of png file to save plot
nameSavePlot2 = configName+'line006U_ReynoldsStressProfile.png' # name of png file to save plot

Ncase = -1
maxCase=len(caseList)

for case in caseList:
	Ncase = Ncase + 1
	sol = basepath + case
	### LOAD OpenFOAM RESULTS ###
	[xcOF,uucOF,ylOF,uulOF] = readOFprofiles(sol,timeList[Ncase])

	### PLOT PROFILES###
	if Ncase==0:
		fig1 = figure(num=1,figsize=(6,6),dpi=150, facecolor='w', edgecolor='w')
		ax1 = fig1.add_subplot(111)
	keep_data = np.where(np.logical_or(xcOF/D<=-0.5,xcOF/D>=0.5))
	ax1.plot(xcOF[keep_data]/D,uucOF[keep_data]/U0**2,
	          ls = lineList[Ncase], color = colorList[Ncase],
	          label=labelList[Ncase])
	#ax.set_xlim(-3,3)
	if Ncase==maxCase-1:
		ax1.set_ylabel(r'$U/U_0$')
		ax1.grid()
		ax1.set_xlabel(r'$x/D$')
		ax1.legend()
	
	if Ncase==0:
		fig2 = figure(num=2,figsize=(6,6),dpi=150, facecolor='w', edgecolor='w') 
		ax2 = fig2.add_subplot(111)
	ax2.plot(ylOF/D,uulOF/U0**2,
	          ls = lineList[Ncase], color = colorList[Ncase],
	          label=labelList[Ncase])
	#ax2.set_xlim(-3,3)
	if Ncase==maxCase-1:
		ax2.set_ylabel(r'$\overline{u^\prime u^\prime}/U_0^2$')
		ax2.grid()
		ax2.set_xlabel(r'$y/D$')
		ax2.legend()

#fig.tight_layout()
show()

### SAVE FIGURE ###

if savePlot :
    fig1.savefig(pathSavePlot+nameSavePlot1,dpi=150)
    fig2.savefig(pathSavePlot+nameSavePlot2,dpi=150)
