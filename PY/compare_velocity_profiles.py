#!/usr/bin/env python3

'''
This scripts is used to compare velocity profiles between two OpenFOAM simulations.
'''

import os
import sys

import numpy as np
from fluidfoam import readof as rdf
from pylab import figure, subplot, axis, xlabel, ylabel, show, savefig, plot
from pylab import title, matplotlib, legend, grid
#
# Functions
#
def readOFprofiles(pathFoamDir,time_name,Uname):
	pathCenter = pathFoamDir+'postProcessing/linesample/'+time_name+'/center_line_'+Uname+'.xy'
	data = np.loadtxt(pathCenter,unpack=True)
	data = np.array(data)
	xc = data[0,:]
	uc = data[1,:]
	del data
	pathLine006 = pathFoamDir+'postProcessing/linesample/'+time_name+'/line_006_'+Uname+'.xy'
	data = np.loadtxt(pathLine006,unpack=True)
	data = np.array(data)
	yl = data[0,:]
	ul = data[1,:]
	del data
	
	return xc, uc, yl, ul

########## PARAMETERS AND SETTINGS, SET BY USER ###########

# cylinder diameter, velocity compnents to use and bottom surface condition
D = 0.04
U0 = 10

#
configName = 'KWSST'
#configName = 'RANS'
#configName = 'RANS_URANS'
#configName = 'URANS_O1O2'
#configName = 'URANS_LES'

if configName == 'KWSST':
	basepath='../RANS/'
	caseList = ['KWSST/']
	timeList = ['18765/']#'8765']
	UnameList = ['U']
	labelList = ['k-omega SST RANS']
elif configName == 'RANS':
	basepath='../RANS/'
	caseList = ['KWSST/','KEPS/']
	timeList = ['8765','274']
	UnameList = ['U','U']
	labelList = ['k-omega SST RANS','k-epsilon RANS']
elif configName == 'RANS_URANS':
	basepath='../'
	caseList = ['RANS/KWSST/','URANS/ORDER1/KWSST/']
	timeList = ['8765','1']
	UnameList = ['U','UMean_UPrime2Mean']
	labelList = ['k-omega SST RANS','k-omega SST URANS']
elif configName == 'URANS_O1O2':
	basepath='../'
	caseList = ['URANS/ORDER1/KWSST/','URANS/ORDER2/KWSST/']
	timeList = ['1','2']
	UnameList = ['UMean_UPrime2Mean','UMean_UPrime2Mean']
	labelList = ['k-omega SST URANS O1','k-omega SST URANS O2']
elif configName == 'URANS_LES':
	basepath='../'
	caseList = ['URANS/ORDER2/KWSST/','LES/LEScylinder/']
	timeList = ['2','0.63742']
	UnameList = ['UMean_UPrime2Mean','UMean_UPrime2Mean']
	labelList = ['k-omega SST URANS','LES']

lineList = ['-','--','-.',':','-']
colorList = ['r','b','g','m','c']

### saving plot parameters ###
savePlot = False # True if plot to be saved else False
pathSavePlot = '../Figures/' # path of plot for saving
nameSavePlot1 = configName+'centerlineU_velprofile.png' # name of png file to save plot
nameSavePlot2 = configName+'line006U_velprofile.png' # name of png file to save plot

Ncase = -1
maxCase=len(caseList)

for case in caseList:
	Ncase = Ncase + 1
	sol = basepath + case
	### LOAD OpenFOAM RESULTS ###
	[xcOF,ucOF,ylOF,ulOF] = readOFprofiles(sol,timeList[Ncase],UnameList[Ncase])

	### PLOT PROFILES###
	if Ncase==0:
		fig1 = figure(num=1,figsize=(6,6),dpi=150, facecolor='w', edgecolor='w')
		ax1 = fig1.add_subplot(111)
	keep_data = np.where(np.logical_or(xcOF/D<=-0.5,xcOF/D>=0.5))
	ax1.plot(xcOF[keep_data]/D,ucOF[keep_data]/U0, 
	          ls = lineList[Ncase], color = colorList[Ncase],
	          label=labelList[Ncase])
	#ax.set_xlim(-3,3)
	if Ncase==maxCase-1:
		ax1.set_ylabel(r'$U/U_0$')
		ax1.grid()
		ax1.set_xlabel(r'$x/D$')
		ax1.legend()
	
	if Ncase==0:
		fig2 = figure(num=2,figsize=(6,6),dpi=150, facecolor='w', edgecolor='w') 
		ax2 = fig2.add_subplot(111)
	ax2.plot(ylOF/D,ulOF/U0, 
	          ls = lineList[Ncase], color = colorList[Ncase],
	          label=labelList[Ncase])
	#ax2.set_xlim(-3,3)
	if Ncase==maxCase-1:
		ax2.set_ylabel(r'$U/U_0$')
		ax2.grid()
		ax2.set_xlabel(r'$y/D$')
		ax2.legend()

#fig.tight_layout()
show()

### SAVE FIGURE ###

if savePlot :
    fig1.savefig(pathSavePlot+nameSavePlot1,dpi=150)
    fig2.savefig(pathSavePlot+nameSavePlot2,dpi=150)
