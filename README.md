Practical Lab session for TEC21 winter school 2023

Authors: J. CHAUCHAT (LEGI/GINP), C. BONAMY (LEGI/CNRS), M. RENAUD (LEGI/Fondation GINP), A. GILLETTA DE SAINT-JOSEPH (LEGI/FEM)


git clone https://gricad-gitlab.univ-grenoble-alpes.fr/chauchaj/tp_num_tec21


How to start:
- open a terminal
- cd in one of the folder e.g. RANS/KWSST
- type ./Allrun it will 
    * create the blockMeshDict file using generateMesh.py 
    * create the mesh using blockMesh
    * create the folder 0 from 0_org
    * visualize the mesh using visuMesh.py
- you can then run model using simpleFoam for RANS and pimpleFoam for URANS and LES
- the postprocessing can be done using paraview/paraFoam. Two states are provided to visualize 
   the velocity contours and the vorticity contours: contour.pvsm, contourVorticity.pvsm
   the vorticity needs to be postporcessed from the case folder using:
   postProcess -func vorticity -time 0:
- Then, you can use the python scripts provided in the PY folder:
    * PY/compare_velocity_profiles.py       
        -> compare velocity profiles in the plane of symmetry and in the transverse direction (wake region)
    * PY/compare_ReynoldsStress_profiles.py 
        -> compare Reynolds shear stress profiles in the transverse direction (wake region)
    * PY/compare_Strouhal.py              
        -> compare Spectrum of velocity signals (wake region) 

Suggested work:
- Compare 2D RANS and URANS with k-omegaSST model
- Mesh sensitivity - yPlus effect
- Compare first and 2nd order k-omegaSST model
- Compare URANS et LES

Repository organization:

RANS: steadystate solver simpleFoam 

	- KEPS
	- KWSST
    
URANS: unsteady flow solver pimpleFoam

	- ORDER1: Euler/upwind schemes
		* KWSST
		* KEPS 
		
	- ORDER2: Crank-Nicolson/linearUpwind schemes (initialized from ORDER1)
		* KWSST
		* KEPS 
        
LES: unsteady flow solver pimpleFoam

		* RUN1: dynamicLagrangian subgrid model
